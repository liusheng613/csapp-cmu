/*
 * 64-bit clean allocator based on explicit free list. 
 * 
 * In this naive approach, a block is allocated by simply incrementing
 * the brk pointer. There are headers and footers. in alocated blocks.
 *  Blocks are coalesced when free. Blocks must be aligned to doubleword(8 Byte)
 * boundaries.so minimum block size is 16
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in the following struct.
 ********************************************************/
team_t team = {
    /* Team name */
    "liusheng",
    /* First member's full name */
    "sheng",
    /* First member's email address */
    "liusheng613@126.com",
    /* Second member's full name (leave blank if none) */
    "",
    /* Second member's email address (leave blank if none) */
    ""
};

/* single word (4) or double word (8) alignment */
#define ALIGNMENT 8

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)


#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

/* $begin mallocmacros */
/* Basic constants and macros */
#define WSIZE       4       /* Word and header/footer size (bytes) */ //line:vm:mm:beginconst
#define DSIZE       8      /* Double word size (bytes) */
#define CHUNKSIZE  (1<<12)  /* Extend heap by this amount (bytes) */  //line:vm:mm:endconst 

/* return max*/
#define MAX(x, y) ((x) > (y)? (x) : (y))  

/* Pack a size and allocated bit into a word */
#define PACK(size, alloc)  ((size) | (alloc))

/* read and write a word at address p*/
#define GET(p)       (*(unsigned int *)(p))
#define PUT(p, val)  (*(unsigned int *)(p) = (val))

/* read the size and allocated fields from address p,p must be a header or a footer*/
#define GET_SIZE(p)   (GET(p) & ~0x7)
#define GET_ALLOC(p)  (GET(p) & 0x1)

/* given block ptr bp, compute address of its header and footer */
#define HDRP(bp)      ((char *)(bp) - WSIZE)
#define FTRP(bp)      ((char *)(bp) + GET_SIZE(HDRP(bp)) - DSIZE)

/* given block ptr bp,compute address of next and previous blocks */
#define NEXT_BLKP(bp)  ((char *)(bp) + GET_SIZE(((char *)(bp) - WSIZE)))
#define PREV_BLKP(bp)  ((char *)(bp) - GET_SIZE(((char *)(bp) - DSIZE)))

#define PREV_POINT(bp)  ((char *)(bp))
#define NEXT_POINT(bp)  ((char *)(bp) + WSIZE)
/* $end mallocmacros */

/* Global variables */
static char *heap_listp = 0;  /* Pointer to first block */  
static char *free_listp = 0;  /* Pointer to the first free block, 0 means not point to anything*/

/* Function prototypes for internal helper routines */
static void * extend_heap(size_t words);
static void   place(void * bp,size_t asize);
static void * find_fit(size_t asize);
static void * coalesce(void * bp);
// static void * deletefromFreeList(void * bp);
static void printblock(void * bp);
static void checkheap(int verbose);
static void checkblock(void *bp);
static void checkfreelist();

const int  DEBUG = 0;

/* 
 * mm_init - initialize the malloc package.gitsss
 */
int mm_init(void)
{
    /* create the initial empty heap */
    if ((heap_listp = mem_sbrk(4 * WSIZE)) == (void *) -1)
    {
        return -1;
    }

    PUT(heap_listp,0); /* alignment padding */
    PUT(heap_listp + (1 * WSIZE),PACK(DSIZE,1)); //Prologue header
    PUT(heap_listp + (2 * WSIZE),PACK(DSIZE,1)); //Prologue footer
    PUT(heap_listp + (3 * WSIZE),PACK(0,1));     //Epilogue header
    heap_listp += (2 * WSIZE);
    // free_listp = heap_listp + WSIZE; //free list header pointer the Epilogue header

    //extend the empty heap with a free block of CHUNKSIZE bytes
    if (extend_heap(CHUNKSIZE / WSIZE) == NULL)
    {
        return -1;
    }
    return 0;
}

/* 
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 *     Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size)
{
    /* adjusted block  size */
    size_t asize;
    /* amount to extend head if no fit */
    size_t extendsize;
    
    char * bp;

    if (heap_listp == 0)
    {
        mm_init();
    }

    /* Ignore spurious requests */
    if (size == 0)
    {
        return NULL;
    }

    /* Adjust block size to include overhead and alignment reqs. */
    if (size < DSIZE)
    {
        asize = 2 * DSIZE;
    }
    else 
    {
        asize = DSIZE * ((size + (DSIZE) + (DSIZE - 1)) / DSIZE);
    }

    /* Search the free list for a fit */
    if ((bp = find_fit(asize)) != NULL)
    {
        if (DEBUG)
        {
            printf("0000 before place,block bp: %p\n",bp);
            checkblock(bp);
            checkfreelist();
        }

        place(bp,asize);
        if (DEBUG)
        {
            printf("0000 after place,block bp:%p\n",bp);
            checkblock(bp);
            checkfreelist();
        }
        return bp;
    }

    /* No fit found. Get more memory and place the block */
    extendsize = MAX(asize,CHUNKSIZE);

    if ((bp = extend_heap(extendsize / WSIZE)) == NULL)
    {
        return NULL;
    }

    if (DEBUG)
    {
        printf("1111 before place,block bp: %p\n",bp);
        checkblock(bp);
    }

    place(bp,asize);
    if (DEBUG)
    {
        printf("1111 after place,block bp: %p\n",bp);
        checkblock(bp);
        checkfreelist();
    }

    return bp;
}

/*
 * mm_free - Freeing a block does nothing.
 */
void mm_free(void *ptr)
{
    size_t allocted = GET_ALLOC(HDRP(ptr));
    if (!allocted)
    {
        return;
    }

    if (heap_listp == 0)
    {
        mm_init();
    }

    /*free allocated blocks */
    size_t size = GET_SIZE(HDRP(ptr));
    if (size <= 0)
    {
        return;
    }

    PUT(HDRP(ptr),PACK(size,0));
    PUT(FTRP(ptr),PACK(size,0));

    //
    PUT(PREV_POINT(ptr),0);

    coalesce(ptr);
}

/*
 * mm_realloc - Implemented simply in terms of mm_malloc and mm_free
 */
void *mm_realloc(void *ptr, size_t size)
{
    size_t oldsize;
    void * newptr;

    /* If size == 0 then this is just free, and we return NULL. */
    if (size == 0)
    {
        mm_free(ptr);
        return 0;
    }

    /* If oldptr is NULL, then this is just malloc. */
    if (ptr == NULL)
    {
        return mm_malloc(size);
    }

    newptr = mm_malloc(size);
    /* If realloc() fails the original block is left untouched  */
    if (!newptr)
    {
        return 0;
    }

    /* Copy the old data. */
    oldsize = GET_SIZE(HDRP(ptr));
    if (size < oldsize)
    {
        oldsize = size;
    }
    memcpy(newptr,ptr,oldsize);

    /* free the old block */
    mm_free(ptr);

    return newptr;
}

/* 
 * extend_heap - Extend heap with free block and return its block pointer
 */
static void * extend_heap(size_t  words)
{
    if (DEBUG)
    {
        printf("extend_heap words:%d\n",words);
    }
    
    char * bp;
    size_t asize; // size in Byte after adjust for alignment

    //allocate an even number of words to maintain alignment
    asize = (words % 2) ? (words + 1) * WSIZE : words * WSIZE;
    if ((long)(bp = mem_sbrk(asize)) == -1)
    {
        return NULL;
    }

    // printf("extend_heap after mem_sbrk\n");

    //initialize free block header/footer and the epilogue header
    PUT(HDRP(bp), PACK(asize,0)); // Free block header
    PUT(FTRP(bp),PACK(asize,0)); // Free block footer
    PUT(HDRP(NEXT_BLKP(bp)),PACK(0,1)); // new epilogue header 
    if (DEBUG)
    {
        printf("bp:%p,asize:%x,ftrp:%p,epilogue:%p\n",bp,asize,FTRP(bp),HDRP(NEXT_BLKP(bp)));
    }

    //update free list header
    PUT(PREV_POINT(bp), 0); // new free block prev point to header
    PUT(NEXT_POINT(bp), 0);
    // if (free_listp == 0)
    // {
    //     free_listp = bp;
    //     PUT(NEXT_POINT(bp),0); //this block is the only free block,next is nullptr
    // }
    // else
    // {
    //     if (DEBUG)
    //     {
    //         printf("extend_head before set freelist pointer,bp:%p,free_list:%p\n",bp,free_listp);
    //     }
    //     PUT(NEXT_POINT(bp),free_listp); //new free block next point to pre header
    //     PUT(PREV_POINT(free_listp), bp); // prev header pointer point to the new block
    //     free_listp = bp;

    //     if (DEBUG)
    //     {
    //         printf("extend_head after set freelist pointer,bp:%p,free_list:%p\n",bp,free_listp);
    //     }
    // }

    // coalesce if the previous block was free
    void * coalPtr = coalesce(bp);
    if (DEBUG)
    {
        printf("end coalesce\n");
    }
    return coalPtr;
}

/*
coalesce adjacent blocks,bp should be block pointer
*/
static void * coalesce(void * bp)
{
    if (DEBUG)
    {
        printf("coalesce bp:%p\n",bp);
        checkblock(bp);
    }
 
    size_t prev_alloc = GET_ALLOC(FTRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));
    int size = GET_SIZE(HDRP(bp));

    if (DEBUG)
    {
        printf("coalesce bp:%p,size:%d,prev_alloc:%d,next_alloc:%d\n",bp,size,prev_alloc,next_alloc);
        checkblock(bp);
    }

    //case 1:pre and next all allocated
    if (prev_alloc && next_alloc)
    {
        //set freelist point
        if (free_listp == 0)
        {
            PUT(PREV_POINT(bp),0); 
            PUT(NEXT_POINT(bp),0); 
            free_listp = bp;
        }
        else
        {
            //set bp to the free list header
            PUT(PREV_POINT(bp),0);
            PUT(NEXT_POINT(bp),free_listp);
            PUT(PREV_POINT(free_listp),bp);

            free_listp = bp;
        }
        return bp;
    }
    else if (prev_alloc && !next_alloc)
    {
        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));

        void * nextblk = NEXT_BLKP(bp);
        PUT(HDRP(bp), PACK(size, 0));
        PUT(FTRP(nextblk), PACK(size, 0));

        //update free list pointer
        // PUT(PREV_POINT(bp),PREV_POINT(NEXT_BLKP(bp)));
        // PUT(PREV_POINT(NEXT_BLKP(bp)),0);
        PUT(NEXT_POINT(bp),GET(NEXT_POINT(nextblk)));
        void * prevFree = PREV_POINT(bp);
        if (prevFree == 0)
        {
            free_listp = bp;
        }

        void * nextFreeBlk = GET(NEXT_POINT(nextblk));
        if (nextFreeBlk != 0)
        {
            PUT(PREV_POINT(nextFreeBlk),bp);
        }
    }
    else if (!prev_alloc && next_alloc)
    {
        size += GET_SIZE(FTRP(PREV_BLKP(bp)));
        void * prevblk = PREV_BLKP(bp); 
        PUT(HDRP(prevblk),PACK(size, 0));
        PUT(FTRP(bp),PACK(size, 0));

        //update free list pointer
        // PUT(NEXT_POINT(prevblk),GET(NEXT_POINT(bp)));

        bp = PREV_BLKP(bp);
    }
    /* prev block and next block both not allocated */
    else
    {
        size += GET_SIZE(FTRP(PREV_BLKP(bp))) + GET_SIZE(HDRP(NEXT_BLKP(bp)));

        void * prevblk = PREV_BLKP(bp);
        void * nextblk = NEXT_BLKP(bp);
        PUT(HDRP(prevblk),PACK(size, 0));
        PUT(FTRP(nextblk),PACK(size, 0));

        //update free list pointer
        PUT(NEXT_POINT(prevblk),GET(NEXT_POINT(nextblk)));
        // //remove next block(free block) from free list
        // PUT(NEXT_POINT(nextblk),0);
        // PUT(PREV_POINT(nextblk),0);

        bp = prevblk;
    }

    if (DEBUG)
    {
        printf("end coalesce bp:%p\n",bp);
        checkblock(bp);
    }

    return bp;
}

/*
find first fit block in the free list
*/
static void * find_fit(size_t asize)
{
    void * prebp = NULL;
    for (void * bp = free_listp; bp && GET_SIZE(HDRP(bp)) > 0; bp = GET(NEXT_POINT(bp)))
    {
        // if (prebp == bp)
        // {
        //     printf("prevbp == bp = %p\n",bp);
        //     break;
        // }
        unsigned csize = GET_SIZE(HDRP(bp));
        if (csize >= asize)
        {
            if (DEBUG)
            {
                printf("find_fit return bp:%p\n",bp);
            }
            
            return bp;
        }
        // prebp = bp;
    }

    // for (void * bp = heap_listp; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp)) {
    //     if (!GET_ALLOC(HDRP(bp)) && (asize <= GET_SIZE(HDRP(bp)))) {
    //         return bp;
    //     }
    // }

    if (DEBUG)
    {
        printf("find_fit return NULL\n");
    }
    
    return NULL;
}

static void place(void * bp,size_t asize)
{
    size_t csize = GET_SIZE(HDRP(bp));
    if (DEBUG)
    {
        printf("begin place csize:%ld,asize:%ld\n",csize,asize);
    }

    if ((csize - asize) >= (2 * DSIZE))
    {
        PUT(HDRP(bp),PACK(asize,1));
        PUT(FTRP(bp),PACK(asize,1));

        //the remain size become a smaller block
        //set the ramining block's header and footer
        void * nextbp = NEXT_BLKP(bp);
    
        // void * hp = HDRP(nextbp);
        // void * fp = FTRP(nextbp);
        // printf("hp:%p,fp:%p\n",hp,fp);

        PUT(HDRP(nextbp),PACK(csize - asize, 0));
        PUT(FTRP(nextbp),PACK(csize - asize, 0));
        

        //set free list point
        PUT(PREV_POINT(nextbp),GET(PREV_POINT(bp)));
        PUT(PREV_POINT(bp), 0);
        PUT(NEXT_POINT(nextbp),GET(NEXT_POINT(bp)));
        PUT(NEXT_POINT(bp), 0);

        if (free_listp == bp)
        {
            free_listp = nextbp;
        }
    }
    else
    {
        PUT(HDRP(bp),PACK(csize,1));
        PUT(FTRP(bp),PACK(csize,1));

        //free current entire block 
        void * preFreeBlock = GET(PREV_POINT(bp));
        if (preFreeBlock != 0)
        {
            PUT(NEXT_POINT(preFreeBlock),GET(NEXT_POINT(bp)));
        }
        
        void * nextFreeBlock = GET(NEXT_POINT(bp));
        if (nextFreeBlock != 0)
        {
            PUT(PREV_POINT(nextFreeBlock),GET(PREV_POINT(bp)));
        }
        else
        {
            free_listp = 0;
        }
    }

    if (DEBUG)
    {
        checkblock(bp);
        printf("end place\n");
    }
}

static void checkfreelist()
{
    printf("checkfreelist start:\n");
    void * prebp = NULL;
    void * nextbp = NULL;
    for (void * bp = free_listp; bp && GET_SIZE(HDRP(bp)) > 0; bp = nextbp)
    {
        int csize = GET_SIZE(HDRP(bp));
        void * prevblk = GET(PREV_POINT(bp));
        void * nextblk =  GET(NEXT_POINT(bp));
        printf("prevblk:%p,bp:%p,csize:%d,nextblk:%p\n",prevblk,bp,csize,nextblk);
        

        nextbp = GET(NEXT_POINT(bp));
        if (nextbp == bp || prebp == nextblk || prebp == bp)
        {
            printf(" prevbp = %p, bp = %p,nextbp:%p\n",prebp,bp,nextbp);
            break;
        }
        prebp = bp;
    }

    printf("checkfreelist end:\n");
}

static void printblock(void * bp)
{
    size_t hsize,halloc,fsize,falloc;
    checkheap(0);

    hsize = GET_SIZE(HDRP(bp));
    halloc = GET_ALLOC(HDRP(bp));
    fsize = GET_SIZE(FTRP(bp));
    falloc = GET_ALLOC(FTRP(bp));

    if (hsize == 0) {
        printf("%p: EOL\n", bp);
        return;
    }

    printf("%p: header: [%ld:%c] footer: [%ld:%c],prevpoint:%p,nextpoint:%p\n", bp, 
           hsize, (halloc ? 'a' : 'f'), 
           fsize, (falloc ? 'a' : 'f'),PREV_POINT(bp),NEXT_POINT(bp)); 
}

static void checkblock(void * bp)
{
    if ((size_t) bp % 8)
    {
        printf("Error: %p is not doubleword aligned\n", bp);
    }

    if (GET(HDRP(bp)) != GET(FTRP(bp)))
    {
        printf("Error: header does not match footer\n");
    }
}

/* 
 * checkheap - Minimal check of the heap for consistency 
 */
void checkheap(int verbose) 
{
    char * bp = heap_listp;

    if (verbose)
    {
        printf("Heap (%p):\n", heap_listp);
    }

    if (GET_SIZE(HDRP(heap_listp)) != DSIZE || !GET_ALLOC(HDRP(heap_listp)))
    {
        printf("Error: Bad prologue header\n");
    }

    checkblock(heap_listp);

    for (bp = heap_listp; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp))
    {
        if (verbose)
        {
            printblock(bp);
        }
        checkblock(bp);
    }

    if (verbose)
    {
        printblock(bp);
    }
    
    if (GET_SIZE(HDRP(bp)) != 0 || !GET_ALLOC(HDRP(bp)))
    {
        printf("Error: bad epilogue header\n");
    }
}













