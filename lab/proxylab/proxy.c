/*
 * @Author: liusheng
 * @Date: 2022-05-19 23:30:52
 * @LastEditors: liusheng
 * @LastEditTime: 2022-05-31 20:50:16
 * @Description: Csapp Proxy lab: web proxy with cache manage use read-write lock
 * email:liusheng613@126.com
 * Copyright (c) 2022 by liusheng/liusheng, All Rights Reserved. 
 */
#include <stdio.h>
#include "csapp.h"

#define DEBUG 0

void debug(const char * fmt, ...)
{
    if (DEBUG)
    {
        va_list args;
        va_start(args, fmt);
        vprintf(fmt, args);
        va_end(args);
    }
}

typedef struct WebCache
{
    char uri[128];
    char headerContent[256];
    char * webContent;
    int headerLength;
    int contentLength;
    struct WebCache * prev;
    struct WebCache * next;
}WebCache;

static WebCache * head = NULL;
static WebCache * tail = NULL;
static int readcnt = 0;
static sem_t mutexLinkList_read;
static sem_t mutexLinkList_write;

static int curTotalCacheSize = 0;

/* Recommended max cache and object sizes */
#define MAX_CACHE_SIZE 1049000
#define MAX_OBJECT_SIZE 102400

/* You won't lose style points for including this long line in your code */
static const char *user_agent_hdr = "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:10.0.3) Gecko/20120305 Firefox/10.0.3\r\n";

void cacheInit();
WebCache * findObjectInCache(char * uri);
void insertObjectInCache(WebCache * cacheItem);
void gethostportFromuri(char * uri, char * requestHost, char * requestPort,char * reqFileName);
void * processClientCon(void * argvp);

int main(int argc, char * argv[])
{
    int listenfd = 0;
    char hostname[MAXLINE];
    char port[MAXLINE];
    pthread_t tid;

    socklen_t clientlen;
    struct sockaddr_storage clientaddr;

    // listen port is pass by commad line
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
    }
    listenfd = Open_listenfd(argv[1]);
    
    cacheInit();
    while (1) {
        clientlen = sizeof(clientaddr);
        //use head data to avoid multithread race
        int * connfdp = Malloc(sizeof(int));
        *connfdp = Accept(listenfd, (SA*)&clientaddr, &clientlen);
        Getnameinfo((SA *) &clientaddr, clientlen, hostname, MAXLINE, port, MAXLINE, 0);
        printf("Accept connection from (%s, %s)\n", hostname,port);
        Pthread_create(&tid,NULL,processClientCon,connfdp);
     
        // remove to thread
        // Close(connfd);
    }
    
    return 0;
}

void cacheInit()
{
    Sem_init(&mutexLinkList_read,0,1);
    Sem_init(&mutexLinkList_write,0,1);
}

void * processClientCon(void * argvp)
{
    Pthread_detach(Pthread_self());
    int connfd = *((int *) argvp);
    free(argvp);

    debug("processClientCon begin\n");
    char buf[MAXLINE];
    char method[32];
    char uri[128];
    char version[16];

    char requestHost[128];
    char requestPort[8];
    char reqFileName[1024];
    rio_t rio;

    int proxyClientFd;
    rio_t proxyRio;
    WebCache * cacheNode = NULL;

    /* read request line and headers */
    Rio_readinitb(&rio, connfd);
    // first line 
    if (Rio_readlineb(&rio, buf, MAXLINE) != 0)
    {
        sscanf(buf, "%s %s %s", method, uri, version);

        debug("processClientCon buf: %s\n", buf);
    
        memset(requestHost,0,128);
        memset(requestPort,0,8);
        memset(reqFileName,0,1024);
        gethostportFromuri(uri,requestHost,requestPort,reqFileName);

        // only support GET
        if (strcasecmp(method, "GET"))
        {
            clienterror(connfd, method, "501", "Not Implemented",
            "Proxy does not implement this method yet");
            return;
        }

        debug("method:%s uri:%s version:%s\n", method, uri, version);

        debug("processClientCon reqHost:%s,reqPort:%s,reqFileName:%s\n", requestHost, requestPort,reqFileName);

        if ((cacheNode = findObjectInCache(uri)) != NULL)
        {
            debug("find uri:%s in Cache\n",uri);
            //read-write lock
            P(&mutexLinkList_read);
            ++readcnt;
            // First in
            if (readcnt == 1)
            {
                P(&mutexLinkList_write);
            }
            V(&mutexLinkList_read);

            Rio_writen(connfd,cacheNode->headerContent,cacheNode->headerLength);
            Rio_writen(connfd, cacheNode->webContent, cacheNode->contentLength);

            P(&mutexLinkList_read);
            --readcnt;
            // last out
            if (readcnt == 0)
            {
                V(&mutexLinkList_write);
            }
            V(&mutexLinkList_read);

            debug("response client response:%s through cache\n",uri);
            Close(connfd);
            return;
        }

        cacheNode = (WebCache *) malloc(sizeof(WebCache));
        memset(cacheNode->headerContent,0,sizeof(cacheNode->headerLength));
        cacheNode->contentLength = 0;
        cacheNode->headerLength = 0;
        memcpy(cacheNode->uri,uri,sizeof(uri));
        cacheNode->prev = NULL;
        cacheNode->next = NULL;

        proxyClientFd = Open_clientfd(requestHost,requestPort);

        Rio_readinitb(&proxyRio, proxyClientFd);

        sprintf(buf,"%s %s %s\r\n", method,reqFileName,version);

        debug("send to server buf:%s",buf);
        Rio_writen(proxyClientFd,buf,strlen(buf));
    }

    
    while(strcmp(buf, "\r\n"))
    {
        Rio_readlineb(&rio, buf, MAXLINE);
        debug("buf:%s\n",buf);
        if (strstr(buf,"GET"))
        {
            Rio_writen(proxyClientFd,buf,strlen(buf));
        }
    }


    debug("after rio write to proxyClientFd:%d\n", proxyClientFd);

    int length = 0;
    char reponseHeader[1024];

    sprintf(reponseHeader,"Host: %s:%d\r\n",requestHost,requestPort);
    Rio_writen(proxyClientFd, reponseHeader, strlen(reponseHeader));
    memset(reponseHeader,0,1024);
    strcat(reponseHeader,"Proxy-Connection: close\r\n");
    strcat(reponseHeader,"Connection: close\r\n");
    strcat(reponseHeader,user_agent_hdr);
    strcat(reponseHeader,"Accept: */*\r\n");
    Rio_writen(proxyClientFd, reponseHeader, strlen(reponseHeader));

    Rio_writen(proxyClientFd, "\r\n", MAXLINE);
    
    //receive header
    do
    {
        Rio_readlineb(&proxyRio, buf, MAXLINE);
        debug("111Rio_readlineb receive request from real server:%s,len:%d\n", buf,strlen(buf));
        if (strstr(buf, "Content-length:"))
        {
            char * colonIndex = strchr(buf,':');
            if (colonIndex)
            {
                length = atoi(++colonIndex);
                cacheNode->contentLength = length;
            }
            debug("length :%d\n", length);
        }
        Rio_writen(connfd, buf, strlen(buf));
        strcat(cacheNode->headerContent,buf);
    }while(strcmp(buf, "\r\n"));

    cacheNode->headerLength = strlen(cacheNode->headerContent);

    

    //receive body
    char * contentBuf = malloc(length);

    memset(contentBuf,0,length);
    Rio_readnb(&proxyRio, contentBuf, length);
    Rio_writen(connfd, contentBuf, length);

    //if web cache content length is small than MAX_OBJECT_SIZE insert cache to list
    if (cacheNode->contentLength <= MAX_OBJECT_SIZE)
    {
        cacheNode->webContent = (char *)malloc(length);
        memset(cacheNode->webContent,0,length);
        memcpy(cacheNode->webContent,contentBuf,length);
        debug("insert cache:%s\n",cacheNode->uri);
        insertObjectInCache(cacheNode);
    }
    else
    {
        free(cacheNode);
    }
 
    free(contentBuf);
    
    debug("proxy reponseHeader:%s\n",reponseHeader);

    Close(proxyClientFd);
    Close(connfd);
}

WebCache * findObjectInCache(char * uri)
{
    //read-write lock
    P(&mutexLinkList_read);
    ++readcnt;
    // First in
    if (readcnt == 1)
    {
        P(&mutexLinkList_write);
    }
    V(&mutexLinkList_read);

    // printCache();
    WebCache * node = head;
    while (node != NULL)
    {
        debug("node->uri:%s,uri:%s\n", node->uri,uri);
        if(strcmp(node->uri,uri) == 0)
        {
            break;
        }
        node = node->next;
    }

    //last out
    P(&mutexLinkList_read);
    --readcnt;
    if (readcnt == 0)
    {
        V(&mutexLinkList_write);
    }
    V(&mutexLinkList_read);

    return node;
}

void printCache()
{
    debug("begin printCache\n");
    WebCache * node = head;
    while (node != NULL)
    {
        debug("node->uri:%s\n", node->uri);
        node = node->next;
    }
    debug("end printCache\n");
}

void insertObjectInCache(WebCache * cacheItem)
{
    //write lock
    P(&mutexLinkList_write);

    if (curTotalCacheSize >= MAX_CACHE_SIZE)
    {
        if (!removeCacheLRU(cacheItem->contentLength))
        {
            printf("removeCacheLRU for %s length:%d failed\n", cacheItem->uri,cacheItem->contentLength);
        }
    }
    
    if (head == NULL)
    {
        debug("insertObjectInCache head is NUll\n");
        head = cacheItem;
        tail = cacheItem;
    }
    else 
    {
        debug("insertObjectInCache head is not NUll\n");
        WebCache * preHead = head;
        cacheItem->prev = NULL;//set as first node
        cacheItem->next = preHead;

        if (preHead != NULL)
        {
            preHead->prev = cacheItem;
        }
        
        head = cacheItem;
        curTotalCacheSize += cacheItem->contentLength;
    }
    V(&mutexLinkList_write);
}

/*
remove the least recent cache to make room for new cache
*/
int removeCacheLRU(int size)
{
    int remainSize = size;
    //write lock
    P(&mutexLinkList_write);
    WebCache * node = tail;
    WebCache * prev = tail->prev;
    while (node != head && remainSize > 0)
    {
        curTotalCacheSize -= node->contentLength;
        remainSize -= node->contentLength;
        free(node->webContent);
        free(node);

        prev->next = NULL;
        node = prev;
        tail = prev;
        prev = prev->prev;
    }
    
    V(&mutexLinkList_write);
    if (remainSize > 0)
    {
        return 0;
    }

    return 1;
}

/*
 * gethostportFromuri- parse URI into get the request host and port info
 * 
 */
/* $begin gethostportFromuri */
void gethostportFromuri(char * uri, char * requestHost, char * requestPort,char * reqFileName)
{
    char * ptr;

    ptr = strstr(uri,"://");
    debug("gethostportFromuri host:%s\n", ptr + 3);
    char * colon = rindex(uri,':');
    int hostLen = colon - ptr - 3;
    strncpy(requestHost,ptr + 3,hostLen);
    debug("gethostportFromuri host:%s\n", requestHost);
    char * portEndPos = colon + 1;
    while (portEndPos[0] >= '0' && portEndPos[0] <= '9')
    {
        ++portEndPos;
    }
    strncpy(requestPort,colon + 1,portEndPos - colon - 1);
    memcpy(reqFileName,portEndPos,strlen(portEndPos));
    debug("gethostportFromuri port:%s,endPos:%s\n", requestPort,portEndPos);
}

/* $end gethostportFromuri */

/*
 * clienterror - returns an error message to the client
 */
/* $begin clienterror */
void clienterror(int fd, char *cause, char *errnum, 
		 char *shortmsg, char *longmsg) 
{
    char buf[MAXLINE];

    /* Print the HTTP response headers */
    sprintf(buf, "HTTP/1.0 %s %s\r\n", errnum, shortmsg);
    Rio_writen(fd, buf, strlen(buf));
    sprintf(buf, "Content-type: text/html\r\n\r\n");
    Rio_writen(fd, buf, strlen(buf));

    /* Print the HTTP response body */
    sprintf(buf, "<html><title>Tiny Error</title>");
    Rio_writen(fd, buf, strlen(buf));
    sprintf(buf, "<body bgcolor=""ffffff"">\r\n");
    Rio_writen(fd, buf, strlen(buf));
    sprintf(buf, "%s: %s\r\n", errnum, shortmsg);
    Rio_writen(fd, buf, strlen(buf));
    sprintf(buf, "<p>%s: %s\r\n", longmsg, cause);
    Rio_writen(fd, buf, strlen(buf));
    sprintf(buf, "<hr><em>The Tiny Web server</em>\r\n");
    Rio_writen(fd, buf, strlen(buf));
}
/* $end clienterror */
