/*
Author:liusheng
Time:2022年 04月 01日 星期五 15:58:28
*/
#include "cachelab.h"
#include <stdlib.h>
#include <stdio.h>
// #include <climits.h>
#include <unistd.h>
#include <string.h>

typedef struct CacheLine
{
    char valid;
    long tag;// t bit tag
    int timestamp;// for LRU counter
    // int blockid;
    // int sets;
}CacheLine;

int s = 0;
int E = 0;
int b = 0;

int hitCount = 0;
int missCount = 0;
int evictionCount = 0;

int opt;
int verbose = 0;
char fileName[64] = {0};
unsigned long globalTimeStamp = 0;

CacheLine ** sets = NULL;

int CacheFind(long S,long tag)
{
    ++globalTimeStamp;
    for(int i = 0; i < E; ++i)
    {
        if(sets[S][i].valid && sets[S][i].tag == tag)
        {
            //update timestamp
            sets[S][i].timestamp = globalTimeStamp;
            ++hitCount;
            if(verbose)
            {
                printf("hit ");
            }  
            return 1;
        }
    }

    ++missCount;

    //fill cache
    //insert to the empty line(cold miss)
    for(int i = 0; i < E; ++i)
    {
        if(sets[S][i].valid == 0)
        {
            sets[S][i].valid = 1;
            sets[S][i].tag = tag;
            //update timestamp
            sets[S][i].timestamp = globalTimeStamp;
            if(verbose)
            {
                printf("miss ");
            }
            return 0;
        }
    }

    //if no empty line,do eviction
    //use LRU strategy,find the smallest timestamp
    int minStamp = sets[S][0].timestamp;
    int minIndex = 0;
    for(int i = 1;i < E; ++i)
    {
        if(sets[S][i].timestamp < minStamp)
        {
            minStamp = sets[S][i].timestamp;
            minIndex = i;
        }
    }

    sets[S][minIndex].valid = 1;
    sets[S][minIndex].tag = tag;
    sets[S][minIndex].timestamp = globalTimeStamp; //start initial
    ++evictionCount;
    if(verbose)
    {
        printf("miss evictions ");
    }
    
    return 0;
}

int main(int argc,char *argv[])
{
    while ((opt = getopt(argc,argv,"hvs:E:b:t:")) != -1)
    {
        switch (opt)
        {
            case 'h':
                printf("usage: -h Optional help flag that prints usage info\n");
                printf("-v Optional verbose flag that displays trace info \n");
                printf("-s <s>: Number of set index bits (S = 2 ^ s is the number of sets)\n");
                printf("-E <E>: Associativity (number of lines per set)\n");
                printf("-b <b>: Number of block bits (B = 2 ^ b is the block size)\n");
                printf("-t <tracefile>: Name of the valgrind trace to replay\n");
                return 0;
                /* code */
            case 'v':
                verbose = 1;
                break;   
            case 's':
                s = atoi(optarg);
                break;
            case 'E':
                E = atoi(optarg);
                break;
            case 'b':
                b = atoi(optarg);
                break;
            case 't':
                strcpy(fileName,optarg);
                break;
            default:
                break;
        }
    }

    sets = (CacheLine * *) malloc(sizeof(CacheLine **) * (2 << s));
    for(int i = 0; i < (2 << s); ++i)
    {
        sets[i] = (CacheLine *) malloc(sizeof(CacheLine) * E);
        memset(sets[i],0,sizeof(CacheLine));
    }

    FILE * fp = fopen(fileName,"r");
    if(fp == NULL)
    {
        printf("Can not open file:%s\n",fileName);
        return 1;
    }

    char access_type;
    unsigned long address;
    int size;
    while(fscanf(fp," %c %lx, %d", &access_type, &address,&size) > 0)
    {
        if(access_type == 'I')
        {
            continue;
        }

        long setNum = (address << (64 - s - b)) >> (64-s);
        long tag = address >> (s + b);
        // unsigned int blockId = address << (64 - b) >> (64 - b);
        if(verbose)
        {
            printf("%c %lx,%d setNum:%lx,tag:%lx  ",access_type,address,size,setNum,tag);
            // printf("%c %lx,%d setNum:%lx,tag:%lx,blockId=%d  ",access_type,address,size,setNum,tag,blockId);
            // printf("%c %lx,%d ",access_type,address,size);
        }
        
        switch(access_type)
        {
            case 'M':
                CacheFind(setNum,tag);
            case 'L':
            case 'S':
                CacheFind(setNum,tag);
            break;
        }
        if(verbose)
        {
            printf("\n");
        }
    }

    fclose(fp);

    //release the malloc alloc memory
    for(int i = 0; i < (2 << s); ++i)
    {
        free(sets[i]);
    }

    free(sets);

    printSummary(hitCount, missCount, evictionCount);
    return 0;
}
