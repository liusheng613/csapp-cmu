/* 
Author:liusheng
Time:2022年 04月 06日 星期三 11:03:18 CST

 * trans.c - Matrix transpose B = A^T
 *
 * Each transpose function must have a prototype of the form:
 * void trans(int M, int N, int A[N][M], int B[M][N]);
 *
 * A transpose function is evaluated by counting the number of misses
 * on a 1KB direct mapped cache with a block size of 32 bytes.
 */ 
#include <stdio.h>
#include "cachelab.h"

int is_transpose(int M, int N, int A[N][M], int B[M][N]);

/* 
 * transpose_submit - This is the solution transpose function that you
 *     will be graded on for Part B of the assignment. Do not change
 *     the description string "Transpose submission", as the driver
 *     searches for that string to identify the transpose function to
 *     be graded. 



 reading from test-trans,we can see s = 5,E = 1,b = 5
 the three test case:
 32 × 32 (M = 32, N = 32)
 64 × 64 (M = 64, N = 64)
 61 × 67 (M = 61, N = 67)

 b = 5,every line can cache 4 int
 s = 5,there are 32 set,every set has 1 line
 */

 /*
 对于32*32的矩阵，每一行有32个整型变量，所以每间隔256/32=8行就会发生如上所说的情况，
 所以需要将矩阵大小控制在8*8以内，以8*8规模的矩阵逐步转置32*32的矩阵，最终将其完全转置；
 对于64*64的矩阵，则是每间隔4行就会发生如上所述情况。对于61*67的矩阵，由于矩阵本身大小并不是2的次方，
 所以间隔8行的数据并不会发生替换，因为不是属于同一组，而Cache能够存放的行数还是较多的，
 只要不是存放在同一行Cache能够存放下更多的数据，所以可以将每次转置的矩阵规模变大一些保证不超过32的前提
 下找一个合适的值就可以了，在本设计中取的值是16。

 即如果存在A和B的数据存放在同一组的情况时，如果需要访问其中一个的数据就会造成替换，采取的解决办法是通过
 临时变量的方式进行中转。结合第一个不足之处的解决方案，只讨论8*8的情况，因为所有的转置都是按照8*8的大小
 重复进行的。在矩阵的行数以及列数一定的情况下，只有对角线上的元素才会存在在A、B对应位置的数据属于同一组
 的情况，因为对角线上数据不论按行优先还是列优先计算偏移，对于行数和列数一样的矩阵来说其偏移量都是一样的，
 所以需要对含有对角线元素的矩阵进行特殊处理。处理的方式是通过临时变量的方式，在转置的过程中，外层循环是8*8矩阵
 的行号，内层循环是8*8矩阵的列号，转置A的一行的时候，如果直接将A对角线上元素赋值给B的对角线上元素时，B的对应
 块就会把A的该行替换掉，然后在A之后的赋值中又得替换回来，所以采取的方式是用临时变量暂存A对角线元素，然后
 待该行循环结束之后再赋值给B的对应位置，这个时候B还是会替换掉A的该行，但是已经不影响了，因为A该行的其他变量都
 已经赋值给B对应的位置了。
 */

 
char transpose_submit_desc[] = "Transpose submission";
void transpose_submit(int M, int N, int A[N][M], int B[M][N])
{
    int blockForRow, blockForCol, tmp;
    int temp = 0;
    int diagonal = 0;

    int blocksize = 0;

    //for N = 64 to temp store value
    int ttemp[8];

    //block = 2 ^ 5 = 32
    //set num: 2 ^ 5 = 32
    //total cache: 32 * 32 = 256
    //every block can hold 32 / 4 = 8 int
    if(N == 32)
    {
        blocksize = 8;
    }
    else if(N == 64)
    {
        blocksize = 8;
    }
    else
    {
        blocksize = 16;
    }

    //reference solution : https://xjdkc.github.io/CMU%20CacheLab.html
    //Cache-Oblivious Algorithms https://en.wikipedia.org/wiki/Cache-oblivious_algorithm
    if(N == 64)
    {
        for (blockForRow=0;blockForRow<N;blockForRow+=8)
        {
            for (int blockForCol = 0;blockForCol < M;blockForCol+=8)
            {
                for (int k=blockForRow;k<blockForRow+4;k++)
                {
                    for (int l=0;l<8;l++)
                        ttemp[l]=A[k][blockForCol+l];
                    for (int l=0;l<4;l++)
                    {
                        B[blockForCol+l][k] = ttemp[l];
                        B[blockForCol+l][k+4] = ttemp[l+4];
                    }
                }
                for (int k=blockForCol+4;k < blockForCol + 8; k++)
                {
                    for (int l = 0; l < 4;l++)
                        ttemp[l]=B[k-4][blockForRow+l+4];
                    for (int l = 4;l < 8;l++)
                        ttemp[l]=A[blockForRow+l][k];

                    for (int l = 4;l < 8;l++)
                        B[k-4][blockForRow+l]=A[blockForRow+l][k-4];
                    for (int l=0;l<8;l++)
                        B[k][blockForRow+l]=ttemp[l];
                }
            }
        }
    }
    else 
    {
        for (blockForRow = 0; blockForRow < N; blockForRow += blocksize) {
            for (blockForCol = 0; blockForCol < M; blockForCol += blocksize) {
                // transpose the block beginning at [i,j]
                for (int r = blockForRow; (r < N) && (r < blockForRow + blocksize); ++r) {
                    for (int c = blockForCol; (c < M) && (c < blockForCol + blocksize); ++c)
                    { 
                        if(r != c)
                        {
                            //A[r][c] miss for first time
                            tmp = A[r][c];
                            //miss for initial col,other will be hit
                            B[c][r] = tmp;
                        }
                        // row is equal to col
                        else
                        {
                            //prevent from B influence A's cache
                            temp = A[r][c];
                            diagonal = r;
                        }
                    }

                    if(blockForRow == blockForCol)
                    {
                        B[diagonal][diagonal] = temp;
                    }
                }
            }
        } 
    } 
}

/* 
 * You can define additional transpose functions below. We've defined
 * a simple one below to help you get started. 
 */ 

/* 
 * trans - A simple baseline transpose function, not optimized for the cache.
 */
char trans_desc[] = "Simple row-wise scan transpose";
void trans(int M, int N, int A[N][M], int B[M][N])
{
    int i, j, tmp;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            tmp = A[i][j];
            B[j][i] = tmp;
        }
    }    

}

/*
 * registerFunctions - This function registers your transpose
 *     functions with the driver.  At runtime, the driver will
 *     evaluate each of the registered functions and summarize their
 *     performance. This is a handy way to experiment with different
 *     transpose strategies.
 */
void registerFunctions()
{
    /* Register your solution function */
    registerTransFunction(transpose_submit, transpose_submit_desc); 

    /* Register any additional transpose functions */
    registerTransFunction(trans, trans_desc); 

}

/* 
 * is_transpose - This helper function checks if B is the transpose of
 *     A. You can check the correctness of your transpose by calling
 *     it before returning from the transpose function.
 */
int is_transpose(int M, int N, int A[N][M], int B[M][N])
{
    int i, j;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; ++j) {
            if (A[i][j] != B[j][i]) {
                return 0;
            }
        }
    }
    return 1;
}

